export const projects = [
  {
    title: 'Skip Logistics',
    description: "Using React, Node.js, Express & MongoDB you'll learn how to build a Full Stack MERN Application - from start to finish. The App is called Memories and it is a simple social media app that allows users to post interesting events that happened in their lives.",
      image: '/images/1.png',
      tags: ['Mongo', 'Express', 'React', 'Next.js', 'Node'],
    source: 'https://skiplogistics.com',
    visit: 'https://gitlab.com/codeandhardware/skipnext',
    id: 0,
  },
  {
    title: 'E-Commerce',
    description:"While building it you're going to learn many advanced React & JavaScript topics, as well as how to use Stripe for card transactions. On top of that, at the end of the video, you will have this unique and complex webshop app that you will be able to add to your portfolio. And trust me, e-commerce applications are impressive.",
    image: '/images/2.png',
    tags: ['React', 'JavaScript'],
    source: 'https://google.com',
    visit: 'https://google.com',
    id: 1,
  },
];

export const TimeLineData = [
  { year: 2011, text: 'Started my journey as wordpress developer', },
  { year: 2015, text: 'Move to Panama and start as a freelance developer', },
  { year: 2016, text: 'My first job as Front-end Developer', },
  { year: 2018, text: 'My first fullstack app development', },
  { year: 2019, text: 'First Project using Javascript Frameworks (Angular)', },
  { year: 2020, text: 'First Big bank related project was deploy to production', },
  { year: 2021, text: 'Starting to create more apps in Next.js, Gatsby, and more', },

];